#include <linux/module.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/init.h>
#include <linux/cdev.h>

#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/io.h>
#include <linux/sched.h>
#include <linux/interrupt.h>

#include <linux/list.h>
#include <linux/irq.h>
#include <linux/slab.h>
#include <linux/gpio.h>
#include <linux/platform_device.h>
#include <mach/platform.h>
#include <mach/gpio.h>
#include <linux/time.h>
#include <linux/delay.h>

// ------ START: GPIO DEFINE -----
#define BCM2708_PERI_BASE       0x20000000
#define GPIO_BASE               (BCM2708_PERI_BASE + 0x200000)    // GPIO controller
#define INP_GPIO(g)   *(gpio.addr + ((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g)   *(gpio.addr + ((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio.addr + (((g)/10))) |= (((a)<=3?(a) + 4:(a)==4?3:2)<<(((g)%10)*3))
#define GPIO_SET  *(gpio.addr + 7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR  *(gpio.addr + 10) // clears bits which are 1 ignores bits which are 0
#define GPIO_READ(g)  *(gpio.addr + 13) &= (1<<(g))
struct bcm2835_peripheral {
    unsigned long addr_p;
    int mem_fd;
    void *map;
    volatile unsigned int *addr;
};

struct bcm2835_peripheral gpio = {GPIO_BASE};
// ------ END: GPIO DEFINE -----

#define LED0_PIN 22
#define LED1_PIN 27

#define MY_MAJOR  200
#define MY_MINOR  0
#define MY_DEV_COUNT 2

#define GPIO_ANY_GPIO_DEVICE_DESC    "myLED"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ITtraining.com.tw");
MODULE_DESCRIPTION("A Simple GPIO Device Driver module for RaspPi");

static int     my_open( struct inode *, struct file * );
static ssize_t my_read( struct file * ,        char *  , size_t, loff_t *);
static ssize_t my_write(struct file * , const  char *  , size_t, loff_t *);
static int     my_close(struct inode *, struct file * );

struct file_operations my_fops = {
        read    :       my_read,
        write   :       my_write,
        open    :       my_open,
        release :       my_close,
        owner   :       THIS_MODULE
};

#define MSG_SIZE 32
static char *msg = NULL;
struct cdev  my_cdev;


//-- INIT_MODULE -- MODULE START--
int init_module(void)
{

    dev_t devno;
    unsigned int count = MY_DEV_COUNT;
    int err;

    devno = MKDEV(MY_MAJOR, MY_MINOR);
    register_chrdev_region(devno, count , "myLED");

    // -- initial the char device
    cdev_init(&my_cdev, &my_fops);
    my_cdev.owner = THIS_MODULE;
    err = cdev_add(&my_cdev, devno, count);

    if (err < 0)
    {
        printk("Device Add Error\n");
        return -1;
    }

    // -- print message
    printk("module gpio : Hello World. This is myLED Driver.\n");
    printk("'mknod /dev/myLED0 c %d 0'.\n", MY_MAJOR);
    printk("'mknod /dev/myLED1 c %d 1'.\n", MY_MAJOR);

    // -- GPIO initial
    gpio.map     = ioremap(GPIO_BASE, 4096);
    gpio.addr    = (volatile unsigned int *)gpio.map;

    // -- message buffer
    printk("module gpio : msg malloc\n");
    msg          = (char *)kmalloc(MSG_SIZE, GFP_KERNEL);
    if (msg !=NULL)
        printk("malloc allocator address: 0x%p\n", msg);

    printk("module gpio : LED GPIO Init\n");
    INP_GPIO(LED0_PIN);
    OUT_GPIO(LED0_PIN);
    INP_GPIO(LED1_PIN);
    OUT_GPIO(LED1_PIN);
    return 0;
}


//-- CLEANUP_MODULE -- MODULE END --
void cleanup_module(void)
{
    dev_t devno;
    printk("module gpio: cleanup_moudle() \n");

    GPIO_CLR = 1 << LED0_PIN;
    GPIO_CLR = 1 << LED1_PIN;
    devno = MKDEV(MY_MAJOR, MY_MINOR);

    /* if the timer was mapped (final step of successful module init) */
    if (gpio.addr){
        /* release the mapping */
        printk("module gpio: release gpio.addr\n");
        iounmap(gpio.addr);
    }
    if (msg){
        /* release the malloc */
        printk("module gpio: release msg\n");
        kfree(msg);
    }
    printk("module gpio: unregister chardev\n");
    unregister_chrdev_region(devno, MY_DEV_COUNT);
    cdev_del(&my_cdev);
}


// -- FILE OPERATION: OPEN --
static int my_open(struct inode *inod, struct file *fil)
{
    int major;
    int minor;
    major = imajor(inod);
    minor = iminor(inod);
    printk("module gpio: FILE OPEN MAJOR: %d MINOR: %d \n",major, minor);
    return 0;
}


// -- FILE OPERATION: READ --
static ssize_t my_read(struct file *filp, char *buff, size_t len, loff_t *off)
{
    int major, minor;
    char led_value;
    short count;

    major = MAJOR(filp->f_dentry->d_inode->i_rdev);
    minor = MINOR(filp->f_dentry->d_inode->i_rdev);

    switch(minor){
        case 0:
            led_value = GPIO_READ(LED0_PIN);
            msg[0] = led_value;
            len = 1;
            break;
        case 1:
            led_value = GPIO_READ(LED1_PIN);
            msg[0] = led_value;
            len = 1;
            break;
        default:
            led_value = -1;
            len = 0;
    }
    count = copy_to_user(buff, msg, len);
    printk("GPIO%d=%d, GPIO%d=%d\n",LED0_PIN ,GPIO_READ(LED0_PIN),LED1_PIN,GPIO_READ(LED1_PIN));
    return 0;
}


// -- FILE OPERATION: WRITE --
static ssize_t my_write(struct file *filp, const char *buff, size_t len, loff_t *off)
{
    int minor;
    short count;

    memset(msg, 0, MSG_SIZE);
    // -- need to get the device minor number because we have two devices
    minor = MINOR(filp->f_dentry->d_inode->i_rdev);
    // -- copy the string from the user space program which open and write this device
    count = copy_from_user( msg, buff, len );

    switch( minor )
    {
        case 0:
            if(msg[0] == '1')      GPIO_SET = 1 << LED0_PIN;
            else if (msg[0] == '0')GPIO_CLR = 1 << LED0_PIN;
            break;
        case 1:
            if(msg[0] == '1')      GPIO_SET = 1 << LED1_PIN;
            else if (msg[0] == '0')GPIO_CLR = 1 << LED1_PIN;
            break;
    }
}


// -- FILE OPERATION: CLOSE --
static int my_close(struct inode *inod, struct file *fil)
{
    int major, minor;
    minor = MINOR(fil->f_dentry->d_inode->i_rdev);
    major = MAJOR(fil->f_dentry->d_inode->i_rdev);
    printk("module gpio: CLOSE MAJOR %d MINOR %d\n",major, minor);
    return 0;
}