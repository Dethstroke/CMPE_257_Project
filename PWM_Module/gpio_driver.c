#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/ioport.h>
#include <asm/gpio.h>
#include <linux/sched.h>
#include <linux/gpio.h>

#include <linux/init.h>
#include <linux/kernel.h>

// #include <stdio.h>
// #include <stdlib.h>
// #include <limits.h>
// #include <unistd.h>
#include <linux/mman.h>



MODULE_LICENSE("GPL");

#define SUCCESS 0
#define DEVICE_NAME     "gpio_driver"
#define BUF_LEN 80
#define GPIO_num        4
#define DEVICE_MAJOR    240


MODULE_DESCRIPTION("GPIO driver");
MODULE_AUTHOR("RRV");

static unsigned long procfs_buffer_size = 0;
static char buffer_data[3];
static char led_status = 0;

/*// These are vars used for the PWM Driver/*/
volatile unsigned *clk, *pwm, *gpio; // pointers to the memory mapped sections 
// static const int BCM2708_PERI_BASE = 0x3F000000;
static const int PWM_BASE = (0x3F000000 + 0x20C000); /* PWM controller */
static const int CLOCK_BASE = (0x3F000000 + 0x101000); /* Clock controller */
static const int GPIO_BASE = (0x3F000000 + 0x200000); /* GPIO controller */
static const int SIZE = 208;
/*// These are vars used for the PWM Driver/*/

static int dev_open(struct inode *inode,struct file *file);
static int dev_release(struct inode *inode,struct file *file);
static ssize_t dev_write(struct file *file, const char *buffer, size_t len, loff_t *offset);
// static ssize_t dev_read(struct file *, char *, size_t, loff_t *);

static struct file_operations fops =
{
    .owner          = THIS_MODULE,
    .open           = dev_open,
    .release        = dev_release,
    .write          = dev_write,
};



// This will just set Pin to PWM
void setPWMPin(void)
{
    printk(KERN_ALERT "Setting PWM Pin\n");
    *(gpio+1) &= ~(7 << 24);
    *(gpio+1) |= (2<<24);
}


volatile unsigned * mapRegAddr(unsigned long baseAddr)

{
  // int mem_fd = 0;
  // void *regAddrMap = MAP_FAILED;


    // Create map for coresponding address, use ioremap in kernel space
    // mmap is used in user space
    volatile unsigned int *regAddrMap = (volatile unsigned int *)ioremap(baseAddr, SIZE);

  /* open /dev/mem.....need to run program as root i.e. use sudo or su */
  // if (!mem_fd) {
  //   if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
  //    printk(KERN_ALERT "can't open /dev/mem");
  //     exit (1);
  //   }
  // }
  
  //  /* mmap IO */
  // regAddrMap = mmap(
  //     NULL,             //Any adddress in our space will do
  //     BLOCK_SIZE,       //Map length
  //     PROT_READ|PROT_WRITE|PROT_EXEC,// Enable reading & writting to mapped memory
  //     MAP_SHARED|MAP_LOCKED,       //Shared with other processes
  //     mem_fd,           //File to map
  //     baseAddr         //Offset to base address
  // );
    
  // if (regAddrMap == MAP_FAILED) {
  //     printk(KERN_ALERT "mmap error");
  //     close(mem_fd);
  //     exit (1);
  // }
  
  // if(close(mem_fd) < 0){ //No need to keep mem_fd open after mmap
  //                        //i.e. we can close /dev/mem
  //   printk(KERN_ALERT "couldn't close /dev/mem file descriptor");
  //   exit(1);
  //   }   
  return (volatile unsigned *)regAddrMap;
}


void initialize_PWM(void)
{
    // Begin initializing the PWM Pin
    printk(KERN_ALERT "Initializing PWM\n");

    // perform the memory mapping
    clk = mapRegAddr(CLOCK_BASE);
    gpio = mapRegAddr(GPIO_BASE);
    pwm = mapRegAddr(PWM_BASE);
  
    // Configure the PWM Pin
    setPWMPin();

}

// initialize our driver here
int dev_init(void)
{
    int ret;

    ret = register_chrdev(DEVICE_MAJOR,DEVICE_NAME,&fops);
    if(ret < 0){
        printk(KERN_ALERT "Device registeration failed \n");
    }
    else{
        printk(KERN_ALERT "Device registeration succeed \n");
    }

    ret = gpio_request(GPIO_num,"gpio_test");
    if(ret != 0){
        printk(KERN_ALERT "GPIO%d is not requested\n",GPIO_num);
    }
    else{
        printk(KERN_ALERT "GPIO%d is requested\n",GPIO_num);
    }

    ret = gpio_direction_output(GPIO_num,0);
    if(ret != 0){
        printk(KERN_ALERT "GPIO%d in not set output\n",GPIO_num);
    }
    else{
        printk(KERN_ALERT "GPIO%d is set output and out is low\n",GPIO_num);
    }

    initialize_PWM();

    return 0;
}

void dev_exit(void)
{
        printk(KERN_ALERT "module exit\n");

    gpio_free(GPIO_num);

    unregister_chrdev(DEVICE_MAJOR,DEVICE_NAME);

}



// 
static int dev_open(struct inode *inode,struct file *file)
{
    gpio_set_value(GPIO_num, led_status);

    printk(KERN_ALERT "GPIO%d is being set\n",GPIO_num);

    return 0;
}

static int dev_release(struct inode *inode,struct file *file)
{
    gpio_set_value(GPIO_num, led_status);

    printk(KERN_ALERT "GPIO%d is set low\n",GPIO_num);

    return 0;
}


//This will be where we will set oue PWM value
static ssize_t dev_write(struct file *file, const char *buffer, size_t len, loff_t *offset)
{

    printk(KERN_ALERT "Entering Write Function");
    procfs_buffer_size = len;
    if ( copy_from_user(buffer_data, buffer, procfs_buffer_size) )
    {
        return -EFAULT;
    }

    *offset += len;

    if(buffer_data[0] == '1')
    {
        led_status = 1;

    }
    else if(buffer_data[0] == '0')
    {
        led_status = 0;
        printk(KERN_ALERT "The number 0 was written");
    }
    else
    {
        printk(KERN_ALERT "The number %c was written", buffer_data[0]);
    }

    pr_info("user input string: %s\n",buffer_data);
    pr_info("user input string len: %lu\n",procfs_buffer_size);

    return procfs_buffer_size;
}

module_init(dev_init);
module_exit(dev_exit);
